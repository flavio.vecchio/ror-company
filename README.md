# README

Tengo casi 20 años de experiencia trabajando en Smalltalk con GemStone en Visual Age/Visual Works,
hoy empiezo a recorrer en paralelo el camino de ROR ya que me interesa la tecnologia.

En este proyecto intentare ir volcando las cosas que voy aprendiendo.
Se agradecen comentarios para poder mejorar el codigo, me pueden ver en linkedin a https://www.linkedin.com/in/flaviovecchio/ o al mail flavio.vecchio at gmail.com

# Instalacion

1) Instalar ROR: https://guides.rubyonrails.org/getting_started.html
2) clonar el proyecto
3) Ejecutar rails server
4) Ingresar a http://127.0.0.1:3000/welcome

