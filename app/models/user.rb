class User < ApplicationRecord
    #Las validaciones que se pueden aplicar a un ApplicationRecord salen de consultar:
    #https://guides.rubyonrails.org/active_record_validations.html
    validates :username, presence: true, 
                        uniqueness: { case_sensitive: false }, 
                        length: { minimum: 3, maximum: 25 }
    #todo en mayuscula se entiende como constante
    #regex que valida el formato de un email
    #se pueden validar en rubular.com
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, 
                        uniqueness: { case_sensitive: false }, 
                        length: { maximum: 105 },
                        format: { with: VALID_EMAIL_REGEX }
    #se habilito el gem gem 'bcrypt', '~> 3.1.7' en gemfile
    #la idea es encriptar las password de los usuarios de forma unidireccional
    has_secure_password
end