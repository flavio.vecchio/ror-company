class UsersController < ApplicationController

    def new
        @user = User.new
    end

    def create
        #Sirve para parar el server y debuguear, por ejemplo params
        #byebug
        @user = User.new(user_params)
        if @user.save
            flash[:notice] = "Usuario creado #{@user.username} correctamente"
            redirect_to welcome_path
        else
            render 'new'
        end
    end

    private
    def user_params
        params.require(:user).permit(:username, :email, :password)
    end

end